"""A demo module implementing fib."""
import argparse
import logging

logger = logging.getLogger()


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Compute fibonacci sequence numbers.")
    parser.add_argument("n", type=int, help="To compute the nth element of fib.")
    parser.add_argument("-v", "--verbose", action="count", help="Be verbose", default=0)
    return parser.parse_args()


def fib(n: int) -> int:
    """Return the *n*th element in the Fibonacci sequence."""
    a, b = 1, 1
    if n < 0:
        logger.error("It'll go wrong with a negative number.")
    logger.info(f"In fib, starting with: {a}, {b}")
    for _ in range(n):
        logger.debug(f"In fib, loop {_} with: {a}, {b}")
        a, b = b, a + b
    return a


def main() -> None:
    """The module entry point."""
    args = parse_args()
    logging.basicConfig(level=[logging.ERROR, logging.INFO, logging.DEBUG][args.verbose])
    print(fib(args.n))


if __name__ == "__main__":
    main()
