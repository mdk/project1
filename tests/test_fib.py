from fib import fib

from hypothesis import given
from hypothesis.strategies import integers, floats


def test_fib_start():
    assert fib(0) == fib(1) == 1


@given(integers(min_value=2, max_value=1_000))
def test_fib_next(i):
    assert fib(i) > 1


@given(integers(min_value=2, max_value=1_000))
def test_fib(n):
    assert fib(n - 1) + fib(n - 2) == fib(n)


@given(
    floats(allow_nan=False, allow_infinity=False),
    floats(allow_nan=False, allow_infinity=False),
)
def test_commutativity(a, b):
    assert a + b == b + a
